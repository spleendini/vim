set noai
set nocin
set tw=75
set expandtab
" Needed to remove that annoying trailing line
set bin
set noeol
