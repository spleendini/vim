" Alex Barrett's .vimrc
filetype off
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()
execute pathogen#infect()
filetype plugin indent on

set t_Co=256
colo wombat
"colo inkpot
set nocompatible "enhanced vim powers
set laststatus=2 " Show status line as second-to-last
set statusline=%F%m%r%h%w\ %30.([ff=%{&ff}/ft=%Y]\ [a=\%03.3b/h=\%02.2B]%)\ %40.((%4l,%4v)\ \ \ \ %3p%%\ \ %LL%) " Enhanced status line
" set cursorline " Sorry, too slow

set tabstop=2
set shiftwidth=2
set expandtab
set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
set list "Show invisible charaters the same way TextMate does.

set autoindent
set smartindent
set bs=2
set nu
set mouse=a
set textwidth=0
set showmatch
set hlsearch
set incsearch
set smartcase
set cinkeys=0{,0},0),:,!^F,o,O,e " Use cindent to indent PHP
set cino==s
set cindent
set cpo+=d
set cpo-=n
set tags=./tags/all " Use generate-amm-tags
" $ must be a keyword
" Removed; maybe PHP got smart after all?
" set iskeyword=@,48-57,_,192-255
" bash-style completion (thank goodness)
set wildmenu
set wildmode=list:longest:full
set completeopt=menu,menuone,longest
set backspace=start,indent,eol
set makeprg=php\ -l\ % 
set errorformat=%m\ in\ %f\ on\ line\ %l
set nofoldenable
set modeline

" Swap files are such a PITA
set nobackup
set noswapfile

let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabMappingForward = '<s-tab>'
let g:SuperTabMappingBackward = '<tab>'
let g:SuperTabLongestHighlight = 1
let php_sql_query = 1
let php_htmlInStrings = 1
let php_folding = 0
let mapleader=","

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" I don't need <F1> help
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>

" jj is rare and works great for ESC
inoremap jj <ESC>

" Save a keystroke for executing commands
nnoremap ; :

" Clockwork-specific PHP magic
source ~/.vim/clockwork.vim
source ~/.vim/plugin/Align.vim
source ~/.vim/plugin/AlignMaps.vim
call AlignCtrl( 'p2P2l:','=>', '=', ':' )

syntax enable
" Clear hlsearch highlights
noremap <F12> :let @/=""<CR>  :echo "Highlights Cleared"<CR>
" Toggle Autoindent
noremap <F9> :set autoindent!<CR>:set autoindent?<CR>
" Toggle C-style indentation
noremap <F8> :set paste!<CR>:set paste?<CR>
" Toggle line numbering
noremap <F7> :set number!<CR>:set number?<CR>
nmap ,/ :let @/=""<CR>  :echo "Highlights Cleared"<CR>
nmap ,p :set paste!<CR>:set paste?<CR>
nmap ,n :set number!<CR>:set number?<CR>
nmap ,l :set list!<CR>:set list?<CR>
nmap ,b :ls<CR>
nmap <SPACE> za

" No idea what this is
autocmd BufReadPost *
      \ if line("'\"") > 0 && line ("'\"") <= line("$") |
      \   exe "normal g'\"" |
      \ endif

" CTRL-C to remove a buffer
noremap <c-c> :hide<cr>
inoremap <c-c> <esc>:bd<cr>
" SHIFT-K for a list of buffers in command mode
noremap <s-k> :ls<cr>

" Enable the `fuzzyfinder' for tagged files
"noremap <F5> :FuzzyFinderTaggedFile<cr>

iabbr /** /**<cr> *<cr>**/<esc>0xxxkxx$a¬

function InsertNewCommentBlock()
  return "/**\<cr>\<bs>* Alex Barrett forgot to change this\<cr>*\<cr>* @author Alex Barrett <barrett@clockwork.com>\<cr>
        \* \<cr>* @param   mixed  $changeme\<cr>* @return  void\<cr>
        \\<bs>**/\<cr>"
endfunction

function InsertNewFunction()
  return InsertNewCommentBlock()."\<cr>function"
endfunction

function InsertNewMethod()
  return InsertNewCommentBlock()."\<cr>public function"
endfunction

iabbr <expr> newfunction InsertNewFunction()
iabbr <expr> newmethod InsertNewMethod()

func Eatchar(pat)
  let c = nr2char(getchar(0))
  return ( c =~ a:pat ) ? '' : c
endfunc

"  Prevent svn commit writing from DRIVING ME INSANE
au FileType svncommit set noai
au FileType svncommit set nocin
au FileType svncommit set tw=75

" PSR PHP
au FileType php set tabstop=4
au FileType php set shiftwidth=4

" Js
au FileType js set tabstop=2
au FileType js set shiftwidth=2

let g:debuggerPort = 51001

" Use plus/minus to expand windows
" http://vim.wikia.com/wiki/Fast_window_resizing_with_plus/minus_keys
if bufwinnr(1)
  map + <C-W>+
  map - <C-W>-
endif

" todo bracket
map ,[ :s/^/\[ \] /<CR><esc>:nohlsearch<CR>

" Comments
function! Comment_Lines()
  if getline(".") =~ '^#'
    let hls=@/
    s/^#//
    let @/=hls
  else
    let hls=@/
    s/^/#/
    let @/=hls
  endif
endfunction
map ,c :call Comment_Lines()<CR>

" Map :E to directory explore the current file
command! E Explore %:h

" Check php syntax whenever you save
function! ValPhp()
  let s:results = system( "php -l " . expand("%") )
  if match( s:results, "No syntax errors detected" ) != -1
    return
  else
    echo s:results
  endif
endfunction

augroup PhpAuto
  au!
  au BufWritePost *.php call ValPhp()
augroup END

if &term =~ "xterm.*"
  let &t_ti = &t_ti . "\e[?2004h"
  let &t_te = "\e[?2004l" . &t_te
  function XTermPasteBegin(ret)
    set pastetoggle=<Esc>[201~
    set paste
    return a:ret
  endfunction
  map <expr> <Esc>[200~ XTermPasteBegin("i")
  imap <expr> <Esc>[200~ XTermPasteBegin("")
  cmap <Esc>[200~ <nop>
  cmap <Esc>[201~ <nop>
endif
