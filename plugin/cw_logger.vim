if ! exists("g:cw_logger_devinitials")
	let g:cw_logger_devinitials = "abarrett"
endif

" log
inoremap <c-l> <esc>o$GLOBALS['logger']->log( "<C-r>=g:cw_logger_devinitials<cr>: ", CW_LOG_DEV );<esc>T:a
noremap <c-l> o$GLOBALS['logger']->log( "<C-r>=g:cw_logger_devinitials<cr>: ", CW_LOG_DEV );<esc>T:a

" log_export
inoremap <c-x> <esc>lT$h<c-v>/<Bslash>w<Bslash>W<cr>y:let @/=""<cr>o$GLOBALS['logger']->log_export( <esc>p$a, CW_LOG_DEV, '<C-r>=g:cw_logger_devinitials<cr>: <esc>p$a' );
noremap <c-x> lT$h<c-v>/<Bslash>w<Bslash>W<cr>y:let @/=""<cr>o$GLOBALS['logger']->log_export( <esc>p$a, CW_LOG_DEV, '<C-r>=g:cw_logger_devinitials<cr>: <esc>p$a' );
