" switchmode
" Toggle between window and hidden buffer switching
" Author: Matt Gray <matt@clockwork.net>
"
" Configure by placing the following line in your .vimrc:
" let g:switchmode = "windows" or
" let g:switchmode = "buffers"
"
" By default, switchmode uses "windows" mode

" Allow hidden buffers
set hidden
" Window minimum height
set wmh=0

function MapForBuffers()
	noremap <c-j> :only<cr>:bnext<cr>
	inoremap <c-j> <esc>:only<cr>:bnext<cr>
	noremap <c-k> :only<cr>:bprev<cr>
	inoremap <c-k> <esc>:only<cr>:bprev<cr>
	:only
endfunction

function MapForWindows()
	:unhide
	noremap <c-j> <c-w>j<c-w>_
	inoremap <c-j> <esc><c-w>j<c-w>_
	noremap <c-k> <c-w>k<c-w>_
	inoremap <c-k> <esc><c-w>k<c-w>_
endfunction

function MapSwitchMode()
	if g:switchmode == "windows"
		call MapForWindows()
	else
		call MapForBuffers()
	endif
endfunction

function ToggleSwitchMode()
	if g:switchmode == "windows"
		let g:switchmode = "buffers"
	else
		let g:switchmode = "windows"
	endif
	call MapSwitchMode()
	
endfunction


if ! exists("g:switchmode")
	let g:switchmode = "windows"
endif

call MapSwitchMode()

noremap <F4> :call ToggleSwitchMode()<cr>:echo "switchmode now: " . g:switchmode<cr>
