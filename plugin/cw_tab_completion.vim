" vim.org tip
" Allows tab-completion when not at the beginning of a line.
function InsertTabWrapper()
      let col = col('.') - 1
      if !col || getline('.')[col - 1] !~ '\k'
          return "\<tab>"
      else
          return "\<c-n>"
      endif
endfunction 
inoremap <tab> <c-r>=InsertTabWrapper()<cr>

set completeopt=menu,preview,longest
