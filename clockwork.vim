" Grab the previous variable and yank it, drop log_export line below it.
"inoremap <c-x> <esc>l?\$<cr>y2w:let @/=""<CR>o$GLOBALS['logger']->log_export( <esc>pa, CW_LOG_DEBUG, 'AGB: <esc>pa' );<esc>
"noremap <c-x> l?\$<cr>y2w:let @/=""<CR>o$GLOBALS['logger']->log_export( <esc>pa, CW_LOG_DEBUG, 'AGB: <esc>pa' );<esc>
inoremap <c-@> l?\$<cr>wyw:let @/=""<CR>o$n_<esc>pi  =  count( $<esc>pi );<CR>for ( $i = 0; $i < $n_<esc>pi; ++$i ) {<CR><esc>
" noremap <c-@> l?\$<cr>wyw:let @/=""<CR>o$n_<esc>pi  =  count( $<esc>pi );<CR>for ( $i = 0; $i < $n_<esc>pi; ++$i ) {<CR><esc>

" magic function c-f
imap <c-f> <esc>A/** * FunctionDescription** @param string $varname description of var* @return array description of return type** @author Alex Barrett <alex.barrett@clockwork.net>**/function function_name ( ) {}<esc>kkA	

iabbr ife if ( ) {hhhi
iabbr switche switch ( ) {hhhi
iabbr whilee while ( ) {hhhi
iabbr authore @author  Alex Barrett  <barrett@clockwork.net>
iabbr lpush $GLOBALS['logger']->pushor_level( CW_LOG_DB );
iabbr lpop $GLOBALS['logger']->pop_level( );

"  populate an object with getters / setters based one consecutive lines w/ variable names
command Lazyget :'<,'>s/^\(.*\)$/	function get_\1 ( ) {		return $this->\1;	}/
command Lazyset :'<,'>s/^\(.*\)$/	function set_\1 ( $\1 ) {		$this->\1 = $\1;	}/

"  check php syntax in the last-saved version of the current file
command! Php :echo Check_php_syntax( )
noremap <F6> :Php

function Check_php_syntax( )
  silent write! ~/.vim_php_syntax

  "  run php -l on the last-saved version of the file
  let error_string  =  system( 'php -l ~/.vim_php_syntax' )

  "  deleting the newlines appears to help avoid the 'Hit ENTER or type command to continue' prompt
  let error_string  =  substitute( error_string, "\n", '', 'g' )

  let error_string  =  substitute( error_string, ' in /.*\.vim_php_syntax', '', 'g' )

  "  if there is an error, jump to that line with the cursor
  if ( match(error_string, 'parse error') != -1 )
    "  remove the odd repetition of 'parse error'
    let error_string  =  substitute( error_string, '\(Parse error: \)parse error, \(.*on line \d\+\).*', '\1\2', '' )

    let error_string  =  substitute( error_string, 'in .* on line', 'on line', '' )

    "  extract the line number of the error
    let line_number  =  substitute( error_string, '.*on line \(\d\+\).*', '\1', '' )

    "  move the cursor to the erroneous line
    call cursor( line_number, 0 )
  endif

  call system( 'rm ~/.vim_php_syntax' )

  "  display the error message
  let error_string = strpart( error_string, 0 ) 
  return error_string
endfunction


" This function will find the previous variable w.r.t. the cursor
" and return the entire variable, including [ ] portions.
"
" Matt Gray
" 2005-05-16

function Get_variable_at_cursor( )
  " Get the current line.
  let cur_line = getline( '.' )

  " 0 indexing
  let i  = col( '.' ) - 1
  let endl = col( '$' )

  " find current char
  let cur_char = strpart( cur_line, i, 1 )

  " find first $ backwards
  while i > 0 && cur_char != '$'
    let i = i - 1
    let cur_char = strpart( cur_line, i, 1 )
  endwhile

  " Some paranoia
  if ( i == 0 && cur_char != '$' ) 
    echo "No variable found prior to the cursor."
    return -1
  endif

  if ( match( strpart( cur_line, i, 2 ), '\$['."'".']' ) != -1 )
    echo "May have found regex."
    return -1
  endif

  if ( i > 0 && match( strpart( cur_line, i-1, 2 ), '\\\$' ) != -1 )
    echo "The nearest $ appears to be escaped."
    return -1
  endif

  " Beginning of variable chunk
  let begin = i

  " Keep track of square braces
  let brace_count = 0
  while i < endl
    let i = i + 1
    let cur_char = strpart( cur_line, i, 1 )
    if ( cur_char == '[' )
      " Increment the brace count and pay no heed
      let brace_count = brace_count + 1
      continue
    endif
    " Decrement the brace counter on closing braces
    if ( cur_char == ']' && brace_count > 0 )
      let brace_count = brace_count - 1
    endif
    " Worry about breaking characters only if we are outside braces.
    if ( brace_count == 0 && match( cur_char, '[ =+\-*/&^;%|$!(){}]' ) != -1 )
      break
    endif
  endwhile

  let len = i - begin
  return strpart( cur_line, begin, len )
endfunction
