" Vim color file
" Maintainer:	Matt Gray <matt@clockwork.net>

set background=dark
if version > 580
    hi clear
    if exists("syntax_on")
	syntax reset
    endif
endif

let g:colors_name = "clockwork"

hi Normal 		term=NONE cterm=NONE gui=NONE ctermfg=LightGray ctermbg=Black 
hi Normal 		term=NONE cterm=NONE gui=NONE ctermfg=253 ctermbg=Black 

hi Comment 		term=NONE cterm=NONE ctermfg=Cyan ctermbg=NONE gui=NONE cterm=none
hi Comment		term=bold cterm=NONE ctermfg=Magenta ctermbg=NONE gui=NONE guifg=Blue guibg=NONE
hi Constant		term=underline cterm=NONE ctermfg=Magenta ctermbg=NONE gui=NONE guifg=#ffa0a0 guibg=NONE
hi Constant		term=underline cterm=NONE ctermfg=DarkGreen ctermbg=NONE gui=NONE guifg=Magenta guibg=NONE
hi CursorLine	term=NONE cterm=NONE ctermbg=234 gui=NONE guifg=#ffa0a0 guibg=NONE
hi LineNr		term=NONE cterm=NONE gui=NONE ctermfg=Yellow
hi LineNr		term=NONE cterm=NONE gui=NONE ctermfg=236
hi Special		term=bold cterm=NONE ctermfg=LightRed ctermbg=NONE gui=NONE guifg=Orange guibg=NONE
hi Special		term=bold cterm=NONE ctermfg=DarkMagenta ctermbg=NONE gui=NONE guifg=SlateBlue guibg=NONE
hi Identifier	term=underline cterm=bold ctermfg=Cyan ctermbg=NONE gui=NONE guifg=#40ffff guibg=NONE
hi Identifier	term=underline cterm=NONE ctermfg=DarkCyan ctermbg=NONE gui=NONE guifg=DarkCyan guibg=NONE
hi Statement	term=bold cterm=NONE ctermfg=Yellow ctermbg=NONE gui=bold guifg=#ffff60 guibg=NONE
hi Statement	term=bold cterm=NONE ctermfg=Brown ctermbg=NONE gui=bold guifg=Brown guibg=NONE
hi PreProc		term=underline cterm=NONE ctermfg=LightBlue ctermbg=NONE gui=NONE guifg=#ff80ff guibg=NONE
hi PreProc		term=underline cterm=NONE ctermfg=DarkMagenta ctermbg=NONE gui=NONE guifg=Purple guibg=NONE
hi Type			term=underline cterm=NONE ctermfg=LightGreen ctermbg=NONE gui=bold guifg=#60ff60 guibg=NONE
hi Type			term=underline cterm=NONE ctermfg=DarkGreen ctermbg=NONE gui=bold guifg=SeaGreen guibg=NONE
hi Underlined	term=underline cterm=underline ctermfg=LightBlue gui=underline guifg=#80a0ff
hi Underlined	term=underline cterm=underline ctermfg=DarkMagenta gui=underline guifg=SlateBlue
hi Ignore		term=NONE cterm=NONE ctermfg=black ctermbg=NONE gui=NONE guifg=bg guibg=NONE
hi Ignore		term=NONE cterm=NONE ctermfg=white ctermbg=NONE gui=NONE guifg=bg guibg=NONE
hi Error		term=reverse cterm=NONE ctermfg=White ctermbg=Red gui=NONE guifg=White guibg=Red
hi Todo			term=standout cterm=NONE ctermfg=Black ctermbg=Yellow gui=NONE guifg=Blue guibg=Yellow
